#!/usr/bin/python

import os, sys
import subprocess
import yaml
import argparse
import base64

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--mountPath', action='store', dest='mountPath', required=True)
parser.add_argument('-s', '--secret', action='store', dest='secret', required=True)
parser.add_argument('-k', '--key', action='store', dest='key', required=True)
parser.add_argument('-t', '--team', action='store', dest='team', required=True)
parser.add_argument('-v', '--value', action='store', dest='value')
parser.add_argument('--from-file', action='store', dest='filePath')
parser.add_argument('-u', action='store_true', dest='update')
args = parser.parse_args()

FNULL = open(os.devnull, 'w')
update_mount = True

if args.filePath and args.value:
    print "Use either -v or --from-file, not both"
    sys.exit(127)

try:
    pod_config = subprocess.Popen(["oc", "export", "dc", "jenkins-slave-%s" % (args.team)], stdout=subprocess.PIPE, stderr=FNULL)
    out, err = pod_config.communicate()
    pod_config = yaml.load(out)
except Exception as e:
    print "Unable to load existing pod config"
    print e
    sys.exit(1)

#Mount Options
#print pod_config['spec']['containers'][0]['volumeMounts']
#print {'mountPath': args.mountPath, 'name': args.secret}
#Volume Options
#print pod_config['spec']['volumes']
#print {'secret': {'defaultMode': 420, 'secretName': args.secret}, 'name': args.secret}

err = subprocess.call(["oc", "get", "secret", "%s-%s" % (args.team, args.secret)], stdout=FNULL, stderr=FNULL)
#If this call succeeds that means the secret is already there and we should fail out.
if not err and not args.update:
    print "Secret already exists"
    print "You can force update the existing secret with -u"
    sys.exit(1)
elif not err and args.update:
    secret_config = subprocess.Popen(["oc", "export", "secret", "%s-%s" % (args.team, args.secret)], stdout=subprocess.PIPE, stderr=FNULL)
    out, err = secret_config.communicate()
    secret_config = yaml.load(out)
    if args.value:
        secret_config['data'][args.key] = base64.b64encode(args.value)
    elif args.filePath:
        with open(args.filePath, 'r') as secret_file:
            secret_config['data'][args.key] = base64.b64encode(secret_file.read())

    with open('tmp-secret', 'w+') as tmp_secret:
        tmp_secret.write(yaml.dump(secret_config))

    err = subprocess.call(["oc", "apply", "-f", "tmp-secret"])
    if err:
        print "Unable to update existing secret"
        print e
        sys.exit(1)
    update_mount = False

else:
    err = 0
    try:
        if args.filePath:
            err = subprocess.call(["oc", "create", "secret", "generic", "%s-%s" % (args.team, args.secret), "--from-file=%s=%s" % (args.key, args.filePath)])
        elif args.value:
            err = subprocess.call(["oc", "create", "secret", "generic", "%s-%s" % (args.team, args.secret), "--from-literal=%s=%s" % (args.key, args.value)])
        else:
            print "You must provide a value for the secret"
            sys.exit(1)

    except Exception as e:
        print "Unable to create secret"
        print e
        sys.exit(1)

    if err:
        print "Something went wrong when adding the secret"
        sys.exit(1)

if update_mount:
    pod_config['spec']['template']['spec']['containers'][0]['volumeMounts'].append({'mountPath': args.mountPath, 'name': "%s-%s" % (args.team, args.secret)})
    pod_config['spec']['template']['spec']['volumes'].append({'secret': {'defaultMode': 420, 'secretName': "%s-%s" % (args.team, args.secret)}, 'name': "%s-%s" % (args.team, args.secret)})

    with open('tmp-config', 'w+') as tmpConfig:
        tmpConfig.write(yaml.dump(pod_config))

    subprocess.call(["oc", "apply", "-f", "tmp-config"])
else:
    subprocess.call(["oc", "rollout", "latest", "dc/jenkins-slave-%s" % (args.team)])
